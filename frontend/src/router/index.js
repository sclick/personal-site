import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Post from "../views/Post.vue"
import Posts from "../views/Posts.vue"
import About from "../views/About.vue"

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      title: "James Click"
    },
  },
  {
    path: '/posts/',
    name: 'Posts',
    component: Posts,
    meta: {
      title: "James' Blog"
    },
  },
  {
    path: '/about/',
    name: 'About',
    component: About,
    meta: {
      title: "About James"
    },
  },
  {
    path: '/posts/:slug',
    component: Post,
    meta: {
      title: "James' Blog"
    },
  },
]

const router = new VueRouter({
  mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  next()
})

export default router
