import sys
from flask import Flask, render_template, send_from_directory, jsonify
from flask_flatpages import FlatPages, pygments_style_defs

app = Flask(__name__)
FLATPAGES_EXTENSION = '.md'
FLATPAGES_ROOT = 'content'
FlatPages = FlatPages(app)
POST_DIR = "posts"

app.config.from_object(__name__)

@app.route("/")
def Index():
    path = '{}'.format("home")
    page = FlatPages.get_or_404(path)
    return render_template('basic.html', page=page)

# Pages in content/ that are .md
@app.route('/<name>/')
def rootPage(name):
    path = '{}'.format(name)
    page = FlatPages.get_or_404(path)
    return render_template('basic.html', page=page)

# Get all blogposts
@app.route("/posts/")
def posts():
    posts = [p for p in FlatPages if p.path.startswith(POST_DIR)]
    posts.sort(key=lambda item:item['date'], reverse=False)
    return render_template('list.html', posts=posts, title="Current Posts")

# Blogpost serve
@app.route('/posts/<name>/')
def post(name):
    path = '{}/{}'.format(POST_DIR, name)
    post = FlatPages.get_or_404(path)
    return render_template('post.html', post=post)

# Staticfiles
@app.route('/s/<path:path>')
def serveSupportingFile(path):
    try:
        return send_from_directory('static', path)
    except:
        return jsonify({
            "attemptedFilepath": path,
            "errorCode": 404,
            "errorDescriptor": f"Sorry, I couldn't find a file with the filepath {path} on this site.",
        })

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)