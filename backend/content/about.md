title: About me

# About James Click

James is a true creator at heart. He finds his outlet of choice to be either fiddling in Illustrator for hours on end or tinkering with random development projects when not already working on "official" projects. A few of his projects have migrated into official programs used by people across the nation, [CodeCup](https://playcodecup.com) being a notable highlight.

Raised in Southern California, he had an opportunity to immerse himself in a plethora of cultures and the beauties of diversity. He often found himself working at surf shop popular with visitors to learn more about the personalities of abroad.

Currently, he works with [SRND](https://srnd.org/) as a Graphic Designer to create greater worldwide diversity in Computer Science. Previously working with organizations like [CYMER Digital Studio](https://www.ymca.org/programs/education/mission-valley-digital-studio) to empower at-risk teens with creative outlets, he finds that individuals who have the tools to create will become unstoppable in their passion.