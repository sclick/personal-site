title: Autobiographical Memory Specificity in Dissociative Identity Disorder
author: James Click
date: 08-05-2019

### Note: these are my personal notes of a research article done for an assignment. They may not be accurate, complete, or comprehendable
[Relevant Presentation Slides](https://api.dageek.tech/static/autodid.pdf)

People suffering from depression or a trauma-related disorder may fail to retrieve a specific episode (i.e., an event within a restricted time period) from memory when asked to do so. Instead, they tend to retrieve over-general memories like categories of events (e.g., “every time I visited my grandparents”).

When dealing with trauma-related disorders, studies have focused on PTSD and found lack of memory specificity in the onset and the maintenance of PTSD.

However, studies investigating the link between over-general memory and dissociative symptomatology, including samples with borderline personality disorder, depression, and nonclinical dissociators, yielded mixed results. It should be noted that the studies that did not find a significant relation between dissociation and over-general memory relied on samples with relatively low dissociation scores.

## DID

DID patients experience an extreme form of identity alteration, or the presence of two or more distinct identities or personality states that in turn take control of the person’s behavior.

In trauma identity states, patients focus on traumatic memories, reliving the events and engaging in defensive actions when they feel threatened. In contrast, in what is called “apparently normal” identity states, the patients do not relate the trauma to themselves. Instead, patients are thought to concentrate on daily life functioning in these states and (both consciously and preconsciously) avoid the retrieval of traumatic memories

# Research goals

- The first aim of the current study was to compare memory specificity of apparently normal identity states and trauma identity states.
- Hypothesis: Given that different identities are considered to serve different functions, that is, avoidant responding in the apparently normal identity state and trauma preoccupation in the trauma state, we expected a tendency to retrieve fewer specific memories in the apparently normal identity state. In addition, the patients were expected to retrieve relatively neutral, nontrauma-related, recent memories in this identity state. In the trauma identity state, in contrast, patients were expected to retrieve negatively valenced, trauma-related, and relatively early memories.

# Terms

## Autobiographical Memory

A memory system consisting of episodes recollected from an individual's life, based on:

- Episodic Memory (personal experience, specific objects and/or event experienced at a place and time)
- Semantic Memory(general knowledge and facts)

## [CaRFAX](https://api.dageek.tech/static/carfax.jpg)

**C**apture **a**nd **R**umination, **F**unctional **A**voidance, and impaired e**X**ecutive control

- Capture and Rumination: Rumination is the focused attention on the symptoms of one's distress, and on its possible causes and consequences
- Functional Avoidance:
- Executive Control: the ability to carry out goal-directed behavior using complex mental processes and cognitive abilities

# Participants

## Demographics

- All participants were female
- Age averaged 41, ranging from 22-70
- Use of Medication was allowed
- Located in Netherlands and Belgium (University of Groningen)
- DID or PTSD was always the primary diagnoses
- Mean number of reported identities were 28 (range 4-39, exceptions of 196)
- Recruited from treatment settings (referred by clinician)

## Groups

- 12 diagnosed with DID
    - Patients self-selected two identities, one reporting awareness of traumatic past, and another reporting no memories of personally experienced trauma (Called the trauma identity and Apparently Normal identity)
- 31 Controls
    - Community Volunteers
    - Responded to newspaper advertisement
    - Excludes anyone with childhood abuse or any relevant attention, visual or memory issues
    - Screened for Psychiatric disorders with Mini International Neuropsychiatric Interview (MINI)
- 26 DID "simulating actors" (faking it)
    - Watched a DID documentary
    - Asked to create two identities
        - "Trauma Identity" with childhood sexual abuse
        - "Apparently Normal Identity" that did not acknowledge the abuse
        - Asked to practice switching identities for a week
- 27 diagnosed with PTSD
    - All reported a history of sexual assault starting in childhood

## Diagnoses Verification

- PTSD was verified with Clinician-Administered PTSD scale
- DID was verified with the Dutch version of the Structured Clinical Interview for DSM-IV (*Diagnostic and Statistical Manual for Mental Disorders-Forth Edition)*

# Procedure

Everyone was tested at their individual treatment center, with as standard test environments as circumstantially possible (quiet, private room)

## Series of Events

### Preliminary Meeting

- Consent Form
- Questionnaires in fixed order
    - DES  - Disassociate Experiences Scale
    - BSI - Brief Symptom Inventory
    - TEC - Traumatic Experiences Checklist
    - PSS-SR - PTSD Symptom Scale Self-Report Version
    - PABQ - Posttraumatic Avoidance Behavior Questionnaire
    - AAQ-TS - Trauma-related version of the Acceptant and Action Questionnaire
- DID patients completed this in their "host" identity
- Control Group were not informed the specifics of this research, only on "psychological complaints." Nothing on DID or Trauma aspects of the research. They did the diagnostic screening over the phone and completed the questionnaires a week in advanced, while the experiment meeting was done at a University Laboratory
- Simulated Patients preformed the questionnaires as themselves, and the AMT in both their apparently normal identity and their imagined "trauma" identity

### Experiment Meeting

- AMT - Autobiographical Memory Task
- DID Patients completed AMT twice, once in their Apparently Normal Identity, and once in their Trauma Identity, order in which is done counterbalanced across participants
- **Task**: In the apparently normal identity, patients were asked to retrieve memories of events as experienced in this identity state, not including trauma. In the trauma identity, patients were asked to retrieve memories of the trauma identity, thus including past traumatic experiences.

# Results

## Group Analysis

- Groups did not differ on age.
- The groups did differ significantly on levels of education,
    - with PTSD patients scoring significantly lower compared with healthy controls, and simulators.
    - The DID patients did not differ significantly from controls, simulators, nor PTSD patients.
- On the TEC, the group difference was also significant.
    - The patient groups (DID and PTSD) did not differ significantly, yet, as expected, they scored significantly higher than controls and simulators.
- This pattern was also found for DES dissociative symptoms, PSS-BR posttraumatic stress symptom, AAQ-TS trauma-related experiential avoidance, PABQ avoidance, and BSI-depression.
- The DID and PTSD groups only differed on DES dissociative symptoms, with DID scoring higher.
- Simulators did not differ significantly on any of the measures used compared with control participants

## Result Analysis

- **Cue** Overlap
    - In simulators: 1 cue out of 260 total cues overlapped (0.38%)
    - In DID: 6 cues out of 120 total cues overlapped (5.0%)
    - The identities differed in their responses except for a very small overlap
- Both identities in DID patients did not differ on **memory specificity**. The simulated patients also exhibited this.
- No **categorical memory** differences found between identities for DID patients. Simulated patients exhibited more categorical memories in their trauma identity.
- No **extended memory** differences between identities for DID and simulated patients.
- No **semantic memory** differences between identities for DID and simulated patients.
- **Trauma-relatedness** revealed that memories associated with the trauma identity tended to be trauma-related compared to SN identities' associated memories.
