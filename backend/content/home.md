header: yes

# James Click
Part graphic designer, part developer, with a hint of mint.

### Some things I've done
  - [Revealed an exploit in 2010-2014 Hyundai Infotainment Systems](https://twitter.com/realJamesClick/status/1121995264649072640)
  - [Started one the first Hack Clubs in Southern California](https://hackclub.com/)
  - Developed a worldwide security competition for High Schoolers

### Some Links
 - [About James](about)
 - [Blog](posts)
 - [Twitter](https://twitter.com/realJamesClick)
 - [LinkedIn](https://www.linkedin.com/in/james-c-909b68135/)
 - [GitHub](https://github.com/JamesClick)

Shoot me an email at i@dageek.tech